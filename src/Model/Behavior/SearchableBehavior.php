<?php

namespace Search\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use I18n\Lib\Lang;
use Cake\Core\Configure;

/**
 * Searchable behavior
 */
class SearchableBehavior extends Behavior
{

    protected $_defaultConfig = [
        // Buscadores
        'implementedFinders' => [
            'fulltext' => 'findFulltext',
            'like' => 'findLike',
        ],
    ];

    protected $_contents = [];

    protected $_currentField = 'content';



    /**
     * @param  Event           $event  [description]
     * @param  EntityInterface $entity [description]
     */
    public function afterSave(Event $event, EntityInterface $entity)
    {
        if (!$entity->__saveSearch) {
            return;
        }

        $this->reset();

        $this->_currentField = 'content';
        
        foreach ($this->getConfig('fields') as $field) {
            if (strpos($field, '.') === false) {
                $this->iterateField($this->_table, $entity, $field);
            } else {
                $parts = explode('.', $field);
                $this->iterateAssociation($this->_table, $parts, $entity);
            }
        }

        if (method_exists($entity, 'saveSearch')) {
            foreach (Lang::iso3() as $locale => $name) {
                $text = $entity->saveSearch($locale);

                if ($text) {
                    $this->add($text, $locale);
                }
            }
        }

        if ($this->getConfig('title')) {
            $this->_currentField = 'title';

            foreach ($this->getConfig('title') as $field) {
                if (strpos($field, '.') === false) {
                    $this->iterateField($this->_table, $entity, $field);
                } else {
                    $parts = explode('.', $field);
                    $this->iterateAssociation($this->_table, $parts, $entity);
                }
            }
        }

        $this->save($entity);
    }

    public function afterDelete(Event $event, EntityInterface $entity)
    {
        $this->bindSearch();
        TableRegistry::getTableLocator()->get('Search.Searches')->deleteAll([
            'foreign_key' => $entity->id,
            'model' => $this->_table->getAlias()
        ]);
    }


    private function iterateField($table, $entity, $field)
    {
        if ($table->hasBehavior(Configure::read('I18n.behaviorName')) && $table->hasTranslate($field)) {
            foreach (Lang::iso3() as $locale => $name) {
                $this->add($entity->translation($locale)->$field, $locale);
            }
        } else {
            foreach (Lang::iso3() as $locale => $name) {
                $this->add($entity->$field, $locale);
            }
        }
    }
    /**
     * Añade un texto a la propiedad _contents
     * 
     * @param string $content
     * @param string $locale 
     */
    private function add($content, $locale)
    {
        $this->_contents[$locale][$this->_currentField][] = strip_tags($content);
    }


    private function iterateAssociation($table, $parts, $entity)
    {
        if (count($parts) == 2) {
            $field = $parts[1];
            unset($parts[1]);
        } elseif (count($parts) == 1) {
            $field = $parts[0];
        } else {
            $field = false;
        }

        foreach ($parts as &$property) {
            $association = $table->association($property);

            if ($association && in_array($association->type(), ['oneToMany', 'manyToMany'])) {
                $_property = $association->getProperty();

                if (!$field) {
                    $_parts = array_values(array_splice($parts, 1));
                }

                foreach ((array)$entity->$_property as $_entity) {
                    if ($field) {
                        $this->iterateField($table->{$association->name()}, $_entity, $field);
                    } else {
                        $this->iterateAssociation($table->{$association->name()}, $_parts, $_entity);
                    }
                }
            } else {
                if ($association) {
                    $_entity = $entity->{$association->property()};

                    if ($_entity) {
                        $this->iterateField($association, $_entity, $field);
                    }
                }
            }
        }
    }
    /**
     * Guarda los registros para cada idioma
     * 
     * @param  string $id
     * @return 
     */
    private function save($_entity)
    {
        $this->Searches = TableRegistry::getTableLocator()->get('Search.Searches');

        foreach ($this->_contents as $locale => $content) {
            $data = [
                'foreign_key' => $_entity->id,
                'locale' => $locale,
                'model' => $this->_table->getAlias()
            ];

            $entity = $this->Searches->find()
                ->where($data)
                ->first();

            if (!$entity) {
                $entity = $this->Searches->newEntity($data);
            }

            if (empty($content['content'])) {
                $content['content'] = [];
            }

            $text = implode(" ", $content['content']);
            $text = str_replace(['.', ','], '', $text);
            $entity->set('content', $text);


            if (empty($content['title'])) {
                $content['title'] = [];
            }

            $text = implode(" ", $content['title']);
            $text = str_replace(['.', ','], '', $text);
            $entity->set('title', $text);

            if ($published_at = $this->getConfig('published_at')) {

                if (is_callable($published_at)) {
                    $date = $published_at($_entity);
                } else {
                    $date = $_entity->get($this->getConfig('published_at'));
                }
                
                $entity->set('published_at', $date);
            }

            $this->Searches->save($entity);
        }
    }

    /**
     * Resetea la propiedad _contents
     */
    private function reset()
    {
        $this->_contents = [];
    }


    /**
     * Finder
     * 
     * @param  Query  $query  
     * @param  array  $options
     * @return Query
     */
    public function findFulltext(Query $query, array $options)
    {
        $words = explode(" ", $options['text']);
        $search = '';

        foreach ($words as $word) {
            $search .= "+$word ";
        }

        $this->bindSearch();
        $query
            ->contain('Searches')
            ->where([
                'MATCH(`Searches`.`content`) AGAINST(:search IN BOOLEAN MODE)',
                '`Searches`.`model`' => $this->_table->getAlias()
            ])
            ->bind(':search', $search)
            ->order([
                'MATCH(`Searches`.`content`) AGAINST(:search)' => 'desc'
            ])
            ->formatResults(function ($result) {
                return $result->map(function ($row) {
                    $row->unsetProperty('search');
                    return $row;
                });
            });

        if (empty($options['all'])) {
            $query->where([
                '`Searches`.`locale`' => Lang::current('iso3'),
            ]);
        }
        return $query;
    }

    public function findLike(Query $query, array $options)
    {
        $this->bindSearch();
        $query
            ->contain('Searches')
            ->where([
                '`Searches`.`content` LIKE' => '%' . $options['text'] . '%',
                '`Searches`.`model`' => $this->_table->getAlias()
            ])
            ->formatResults(function ($result) {
                return $result->map(function ($row) {
                    $row->unsetProperty('search');
                    return $row;
                });
            });

        if (empty($options['all'])) {
            $query->where([
                '`Searches`.`locale`' => Lang::current('iso3'),
            ]);
        }
        return $query;
    }

    public function bindSearch()
    {
        $this->_table->hasOne('Searches', [
            'className' => 'Search.Searches',
            'foreignKey' => 'foreign_key',
        ]);
    }
}
