<?php

namespace Search\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Text;
use Search\Model\Entity\SearchEntityTrait;

/**
 * Search Entity
 *
 * @property int $id
 * @property string $locale
 * @property string $model
 * @property string $foreign_key
 * @property string $content
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Search extends Entity
{
    use SearchEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];


    public function result($phrase, $radius = 250, $ending = "")
    {
        if (empty($phrase)) {
            return Text::truncate($this->content, $radius, [
                'exact' => false,
            ]);
        }

        $text = strip_tags(str_replace('</p>', '. ', $this->content));
        $phraseLen = mb_strlen($phrase);

        if ($radius < $phraseLen) {
            $radius = $phraseLen;
        }
        $phrases = explode(' ', $phrase);

        foreach ($phrases as $_phrase) {
            $pos = mb_strpos(strtolower($text), mb_strtolower($phrase));
            if ($pos > -1) break;
        }

        $startPos = 0;

        if ($pos > $radius) {
            $startPos = $pos - $radius;
        }

        $textLen = mb_strlen($text);
        $endPos = $pos + $phraseLen + $radius;

        if ($endPos >= $textLen) {
            $endPos = $textLen;
        }

        $excerpt = mb_substr($text, $startPos, $endPos - $startPos);

        if ($startPos != 0) {
            $excerpt = substr_replace($excerpt, $ending, 0, $phraseLen);
        }

        if ($endPos != $textLen) {
            $excerpt = substr_replace($excerpt, $ending, -$phraseLen);
        }

        foreach ($phrases as $phrase) {
            $excerpt = str_replace($phrase, '<span>' . $phrase . '</span>', $excerpt);
        }

        $excerpt = $this->_removeLastWord($excerpt);
        $excerpt = $this->_removeFirstWord($excerpt);

        $excerpt = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $excerpt);
        return '...' . $excerpt . '...';
    }

    protected static function _removeLastWord($text)
    {
        $spacepos = mb_strrpos($text, ' ');

        if ($spacepos !== false) {
            $lastWord = mb_strrpos($text, $spacepos);

            // Some languages are written without word separation.
            // We recognize a string as a word if it doesn't contain any full-width characters.
            if (mb_strwidth($lastWord) === mb_strlen($lastWord)) {
                $text = mb_substr($text, 0, $spacepos);
            }

            return $text;
        }

        return '';
    }

    protected static function _removeFirstWord($text)
    {
        $spacepos = mb_strpos($text, ' ');

        if ($spacepos !== false) {
            $lastWord = mb_strpos($text, $spacepos);

            // Some languages are written without word separation.
            // We recognize a string as a word if it doesn't contain any full-width characters.
            if (mb_strwidth($lastWord) === mb_strlen($lastWord)) {
                $text = mb_substr($text, $spacepos);
            }

            return $text;
        }

        return '';
    }
}
