<?php
namespace Search\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Search\Model\Behavior\SearchableBehavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Datasource\ConnectionManager;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;
use I18n\Lib\Lang;

class ContentsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->entityClass( 'Search\Test\TestCase\Model\Behavior\Content');
  }

}



class CardsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
    $this->entityClass( 'Search\Test\TestCase\Model\Behavior\Card');
  }
}


class TagsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
  }
}

class RelationsTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Timestamp');
  }
}

class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    'title' => true,
    'body' => true,
    'category_id' => true,
    'content_type' => true,
    'cards' => true,
    'card' => true,
    'categories' => true,
    'category' => true,
    'tags' => true,
  ];
}


class Card extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    '*' => true,
    'title' => true,
    'attributes' => true,
    'content_type' => true,
    'price' => true,
    'content_id' => true,
    'id' => true,
    'categories' => true
  ];
}



class AttributesTable extends Table 
{
  public function initialize(array $options) 
  {
    $this->addBehavior( 'Manager.Crudable');
    $this->entityClass( 'Search\Test\TestCase\Model\Behavior\Attribute');
  }
}

class Attribute extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

  protected $_accessible = [
    '*' => true,
  ];
}


/**
 * Search\Model\Behavior\SearchableBehavior Test Case
 */
class SearchableBehaviorTest extends TestCase
{
  public $fixtures = [
    'plugin.manager.contents',
    'plugin.manager.cards',
    'plugin.manager.attributes',
    'plugin.search.searches',
    'plugin.i18n.languages',
    'plugin.block.rows',
    'plugin.manager.translates',
    'plugin.manager.relations',
    'plugin.manager.tags',
  ];


  /**
   * Test subject
   *
   * @var \Search\Model\Behavior\SearchableBehavior
   */
  public $Searchable;

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp() 
  {
    I18n::locale( 'spa');
    parent::setUp();
    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }


  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->Contents);
    unset($this->Languages);
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function setOneLanguage()
  {
    Lang::set( $this->Languages->find()->where(['id' => 1])->all());
  }


  public function testSaveSimple()
  {
    $this->setLanguages();
    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
      ]
    ]);

    $data = [
      'title' => 'Hola mundo',
      'body' => 'Es fantástico'
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->save( $content);

    $result = $this->Contents->find( 'fulltext', ['text' => 'Hola'])
      ->first()
    ;

    $this->assertEquals( $content->id, $result->id);
  }

  
  public function testSaveSimpleTranslation()
  {
    $this->setLanguages();
    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
        'body'
      ]
    ]);

    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title', 'body']
    ]);

    $data = [
      '_translations' => [
        'spa' => [
          'title' => 'Hola mundo',
          'body' => 'Es fantástico'
        ],
        'eng' => [
          'title' => 'Hello World',
          'body' => 'This fantastic',
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->saveContent( $content);

    $result = $this->Contents->find( 'fulltext', ['text' => 'Hola'])
      ->first();

    $this->assertEquals( $content->id, $result->id);
  }


  public function testSaveHasManySimple()
  {
    $this->setLanguages();

    $this->Contents->hasMany( 'Cards');
    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
        'cards.title'
      ]
    ]);

    $this->Contents->crud->associations(['Cards']);
    $this->Contents->Cards->entityClass( 'Search\Test\TestCase\Model\Behavior\Card');

    $data = [
      'title' => 'Hola',
      'cards' => [
        [
          'title' => 'Hola amigos mios',
        ],
        [
          'title' => 'Gente menuda, menuda gente'
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->saveContent( $content);
    
    $result = $this->Contents->find( 'fulltext', ['text' => 'gente'])
      ->first();

    $this->assertEquals( $content->id, $result->id);
  }

  public function testSaveHasManyTranslations()
  {
    $this->setLanguages();

    $this->Contents->hasMany( 'Cards');
    $this->Contents->Cards->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);

    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
        'cards.title'
      ]
    ]);

    $this->Contents->crud->associations(['Cards']);
    $this->Contents->Cards->entityClass( 'Search\Test\TestCase\Model\Behavior\Card');

    $data = [
      'title' => 'Hola',
      'cards' => [
        [
          '_translations' => [
            'spa' => [
              'title' => 'Amigos del alma'
            ],
            'eng' => [
              'title' => 'Friends of soul'
            ]
          ]
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->saveContent( $content);
    $result = $this->Contents->find( 'fulltext', ['text' => 'alma'])
      ->first();

    $this->assertEquals( $content->id, $result->id);
  }


  public function testSaveHasManyThree()
  {
    $this->setLanguages();

    $this->Contents->hasMany( 'Cards');
    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'cards.attributes.title'
      ]
    ]);

    $this->Contents->crud->associations(['Cards', 'Attributes']);
    $this->Contents->Cards->entityClass( 'Search\Test\TestCase\Model\Behavior\Card');

    $this->Contents->Cards->hasMany( 'Attributes');
    $this->Contents->Cards->Attributes->addBehavior( 'Timestamp');
    $this->Contents->Cards->Attributes->entityClass( 'Search\Test\TestCase\Model\Behavior\Attribute');

    $data = [
      'title' => 'Hola',
      'cards' => [
        [
          'title' => 'Hola Card',
          'attributes' => [
            [
              'title' => 'Hola Attribute del alma'
            ],
            [
              'title' => 'Hola 2 Attribute'
            ]
          ]
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->saveContent( $content);
    
    $result = $this->Contents->find( 'fulltext', ['text' => 'alma'])
      ->first();

    $this->assertEquals( $content->id, $result->id);
  }

  public function testBelongsToMany()
  {
    $this->Contents->belongsToMany( 'Tags', [
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'related_id',
      'joinTable' => 'relations',
    ]);

    $this->Contents->crud->associations(['Tags']);

    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'tags.title'
      ]
    ]);

    $data = [
      'title' => 'Hola',
      'tags' => [
        [
          'title' => 'Economía'
        ],
        [
          'title' => 'Mundo'
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);

    $saved = $this->Contents->saveContent( $content);

    $result = $this->Contents->find( 'fulltext', ['text' => 'Economía'])
      ->first();
    
    $this->assertEquals( $content->id, $result->id);

  }

  public function testDelete()
  {
    $this->setLanguages();
    $this->Contents->addBehavior( 'Search.Searchable', [
      'fields' => [
        'title',
      ]
    ]);

    $data = [
      'title' => 'Hola mundo',
      'body' => 'Es fantástico'
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->save( $content);
  

    $this->Contents->delete( $content);
    $this->Contents->bindSearch();
    $search = $this->Contents->Searches->find()
      ->where([
        'foreign_key' => $content->id,
        'model' => 'Contents'
      ])
      ->first();


    $this->assertEquals( null, $search);
  }

}
