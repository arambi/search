<?php
namespace Search\Shell;

use Cake\Console\Shell;

/**
 * Furbish shell command.
 */
class FurbishShell extends Shell
{

  public function main() 
  {
    if( !empty( $this->args [0]))
    {
      $model = $this->args [0];
    }
    else
    {
      $model = $this->in( 'Indica un model (plugin.model)');
    }

    $this->Model = $this->loadModel( $model);

    $limit = 50;
    $offset = 0;
  
    while( true)
    {
      $query = $this->Model->find()
        ->limit( $limit)
        ->offset( $offset)
        ->order([
          $this->Model->getAlias() . '.'. $this->Model->getPrimaryKey()
        ])
      ;

      $contents = $query->all();

      if( $contents->count() == 0)
      {
        break;
      }
      
      foreach( $contents as $content)
      {
        usleep( 5000);
        $_array = $this->Model->find( 'array')->where([
          $this->Model->alias() .'.id' => $content->id 
        ])->first();
  
        $_content = $this->Model->find( 'content')->where([
          $this->Model->alias() .'.id' => $content->id 
        ])->first();
  
  
        $_content->set( 'created', date( 'Y-m-d H:i:s'));
        $this->Model->patchContent( $_content, $_array);
        $this->Model->saveContent( $_content);
      }

      $offset = $offset + $limit;
    }

  }
}
