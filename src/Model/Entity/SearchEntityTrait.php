<?php 

namespace Search\Model\Entity;

use Cake\Utility\Text;

trait SearchEntityTrait
{
  protected function _getSearchTitle()
  {
    return $this->title;
  }

  protected function _getSearchDescription()
  {
    return trim( strip_tags( $this->body));
  }

  protected function _getSearchAntetitle()
  {
    return null;
  }
  
  public function highlight($text, $phrase, $radius = 100, $ending = "...") 
  { 
      $phraseLen = strlen($phrase); 
      
      if ($radius < $phraseLen) 
      { 
            $radius = $phraseLen; 
       } 

        $phrases = explode (' ',$phrase);

        foreach ($phrases as $phrase) {
            $pos = strpos(strtolower($text), strtolower($phrase)); 
            if ($pos > -1) break;
        }

        $startPos = 0; 
        if ($pos > $radius) { 
            $startPos = $pos - $radius; 
        } 

        $textLen = strlen($text); 

        $endPos = $pos + $phraseLen + $radius; 
        if ($endPos >= $textLen) { 
            $endPos = $textLen; 
        } 

        $excerpt = substr($text, $startPos, $endPos - $startPos); 
        if ($startPos != 0) { 
            $excerpt = substr_replace($excerpt, $ending, 0, $phraseLen); 
        } 

        if ($endPos != $textLen) { 
            $excerpt = substr_replace($excerpt, $ending, -$phraseLen); 
        } 

        return Text::highlight( $excerpt, $phrase); 
   } 
}