<?php

use Migrations\AbstractMigration;

class SearchesPublishedAt extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('searches')
            ->addColumn('published_at', 'date', ['default' => null, 'null' => true])
            ->addIndex(['published_at'])
            ->update();
    }
}
