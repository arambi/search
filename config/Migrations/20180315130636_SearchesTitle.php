<?php
use Migrations\AbstractMigration;

class SearchesTitle extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $searches = $this->table( 'searches', ['engine' => 'MyISAM']);
    $searches
        ->addColumn( 'title', 'text', ['default' => null, 'null' => true])
        ->addIndex( ['title'], ['type' => 'fulltext'])
        ->addIndex( ['title', 'content'], ['type' => 'fulltext'])
        ->save();
  }
}
