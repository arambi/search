<?php

use Phinx\Migration\AbstractMigration;

class Searches extends AbstractMigration
{

  public function change()
  {
    $searches = $this->table( 'searches', ['engine' => 'MyISAM']);
    $searches
        ->addColumn( 'locale', 'string', ['limit' => 5])
        ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
        ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
        ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'created', 'datetime', array('default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
        ->addIndex( ['locale', 'model'], ['unique' => false])
        ->addIndex( ['model', 'foreign_key'], ['unique' => false])
        ->addIndex( ['content'], ['type' => 'fulltext'])
        ->save();
  }
}
